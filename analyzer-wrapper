#!/bin/bash -l

set -euo pipefail

# The following three lines have been added as a workaround when gemnasium-maven is
# used in Docker-in-Docker mode. The workaround is necessary because the orchestrator
# layer from the common package doesn't propagate the environment variables from the
# Dockerfile, nor does it execute the .bashrc script when the container is started.
export ASDF_DATA_DIR="/opt/asdf"
export HOME=/root
. /root/.bashrc

export CI_DEBUG_TRACE=${CI_DEBUG_TRACE:='false'}
export HISTFILESIZE=0
export HISTSIZE=0
export LANG=C.UTF-8

[[ $CI_DEBUG_TRACE == 'true' ]] && echo "$@"

project_dir="${CI_PROJECT_DIR:-${@: -1}}"
[[ -d $project_dir ]] && cd "$project_dir"

function debug_env() {
  pwd
  ls -alh
  env | sort
  asdf current

  java -version
}

switch_to java "adoptopenjdk-${DS_JAVA_VERSION:-11}"
[[ $CI_DEBUG_TRACE == 'true' ]] && debug_env

/analyzer-binary "$@"