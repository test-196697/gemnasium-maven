package main

import (
	"testing"
)

const showIvyReportCommandOutput = `
[info] Loading settings from plugins.sbt ...
[info] Loading global plugins from /root/.sbt/1.0/plugins
[info] Loading project definition from /tmp/project/project
[info] Loading settings from build.sbt ...
[info] Set current project to Hello (in build file:/tmp/project/)
[info] /tmp/project/target/scala-2.12/resolution-cache/reports/com.example-hello_2.12-compile.xml
[success] Total time: 1 s, completed Dec 4, 2019, 2:50:04 PM
`

func Test_extractReportPath(t *testing.T) {
	want := "/tmp/project/target/scala-2.12/resolution-cache/reports/com.example-hello_2.12-compile.xml"
	got, _ := extractReportPath([]byte(showIvyReportCommandOutput))

	if got != want {
		t.Errorf("expected file to be %s but got %s", want, got)
	}
}

func Test_extractReportPathError(t *testing.T) {
	_, err := extractReportPath([]byte(""))
	if err == nil {
		t.Error("expected error on invalid input but got nothing")
	}

	_, err = extractReportPath([]byte("[info] /tmp/project/foo.txt\n[success]last line"))
	if err == nil {
		t.Error("expected error on invalid input but got nothing")
	}
}
