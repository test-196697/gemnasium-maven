package main

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/cacert"
	"os/exec"
	"path"
	"testing"
)

func Test_importKeytoolCommand_returns_Java_8_path(t *testing.T) {
	assert := assert.New(t)
	javaHome := "/opt/asdf/installs/java/adoptopenjdk-8.0.252+9.1"

	expected := exec.Command(
		"keytool",
		"-importcert",
		"-alias", "custom",
		"-file", cacert.DefaultBundlePath,
		"-trustcacerts",
		"-noprompt",
		"-storepass", "changeit",
		"-keystore", path.Join(javaHome, "jre/lib/security/cacerts"),
	)

	assert.Equal(expected, importKeytoolCommand(javaHome))
}

func Test_importKeytoolCommand_returns_Java_11_path(t *testing.T) {
	assert := assert.New(t)
	javaHome := "/opt/asdf/installs/java/adoptopenjdk-11.0.7+10.1"

	expected := exec.Command(
		"keytool",
		"-importcert",
		"-alias", "custom",
		"-file", cacert.DefaultBundlePath,
		"-trustcacerts",
		"-noprompt",
		"-storepass", "changeit",
		"-keystore", path.Join(javaHome, "lib/security/cacerts"),
	)

	assert.Equal(expected, importKeytoolCommand(javaHome))
}

func Test_importKeytoolCommand_returns_Java_13_path(t *testing.T) {
	assert := assert.New(t)
	javaHome := "/opt/asdf/installs/java/adoptopenjdk-13.0.2+8.1"

	expected := exec.Command(
		"keytool",
		"-importcert",
		"-alias", "custom",
		"-file", cacert.DefaultBundlePath,
		"-trustcacerts",
		"-noprompt",
		"-storepass", "changeit",
		"-keystore", path.Join(javaHome, "lib/security/cacerts"),
	)

	assert.Equal(expected, importKeytoolCommand(javaHome))
}

func Test_importKeytoolCommand_returns_Java_14_path(t *testing.T) {
	assert := assert.New(t)
	javaHome := "/opt/asdf/installs/java/adoptopenjdk-14.0.1+7.1"

	expected := exec.Command(
		"keytool",
		"-importcert",
		"-alias", "custom",
		"-file", cacert.DefaultBundlePath,
		"-trustcacerts",
		"-noprompt",
		"-storepass", "changeit",
		"-keystore", path.Join(javaHome, "lib/security/cacerts"),
	)

	assert.Equal(expected, importKeytoolCommand(javaHome))
}
